#!/usr/bin/env bash

# Author: Dmitri Popov, dmpop@linux.com

#######################################################################
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######################################################################

# Replace the example value with the actual URL to the ip.php script
URL="https://127.0.0.1/ip.php"

# Check if there is an Internet connection
ping -c1 google.com &>/dev/null

# If there is an Internet connection,
# obtain the local IP address
# and push it to the ip.php script
if [ $? -eq 0 ]; then
    IP=$(hostname -I | cut -d' ' -f1)
    echo "$IP"
    curl -d ip="$IP" -G "$URL"
fi
