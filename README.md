# Slip

Slip (_Simple and light IP_) obtains a local IP address of a Linux machine and pushes it to a PHP script that displays the IP address as a web page.

## Installation

```
git clone https://gitlab.com/dmpop/slip.git
cd slip
nano ip.sh
```

Edit the default value of the `$URL` variable to point to the location of the _ip.php_ script. Move the _ip.php_ file to the specified location.

    crontab -e

Add the following cronjob:

    */3 * * * * /path/to/slip/ip.sh

## Author

Dmitri Popov [dmpop@linux.com](mailto:dmpop@linux.com)

## License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)
